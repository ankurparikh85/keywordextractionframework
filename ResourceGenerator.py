import os

dir = "C:\Users\dell\Documents\observeai"
ip_dir = "C:\Users\dell\Documents\observeai\manual_transcripts"
c1_file = "21073_v3.txt"

c1_dict = {}

file = open(os.path.join(dir, c1_file))
lines = file.readlines()
file.close()

for line in lines:
    line = line.replace("\n","")
    info = line.split(",")
    print(info[0] + " " + info[1])
    c1_dict[info[0]] = info[1]

print(len(c1_dict))

c1_text_list = []
c1_label_list = []

for filename in os.listdir(ip_dir):
    print(filename)
    fileid = filename.split("CallLog")[0].split("-")[1]
    print(fileid)
    file = open(os.path.join(ip_dir, filename),'r')
    lines = file.readlines()
    file.close()
    text = lines[0]
    print(text)

    if fileid in c1_dict:
        #print("c1")
        label = c1_dict[fileid]
        if "N/A" not in label:
            c1_text_list.append(text)
            c1_label_list.append(label)

print(len(c1_text_list))
print(len(c1_label_list))

content = []
for i in range(0,len(c1_text_list)):
    content.append(c1_text_list[i] + "," + c1_label_list[i] + "\n")

file = open(os.path.join(dir, "data.csv"), 'w')
file.writelines(content)
file.flush()
file.close()
