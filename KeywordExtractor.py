import os
import shutil
from Utils.OptionParser import parse_argument
from Utils.Standard import get_logger
from Utils.ConfigurationReader import read as read_configurations
from Utils import Pipeliner
from Utils.ConfigurationReader import read_single_run_config
from FeatureSelection.Chi2Based import fit as chi2_fit
from FeatureSelection.MIBased import fit as mi_fit
from FeatureSelection.TreeBased import fit as tree_fit
from Search.RegexSearcher import search
from ClassCorrelation.SupportAnalysis import  filterBySupport
from Ranking.Overlap import computeOverlap
from multiprocessing import Pool
from contextlib import closing

def save_results(keywords, dir):
    for key in keywords.keys():
        text = "keyword,importance,in_class_support,other_classes_support\n"
        print(keywords[key])
        for keyword in keywords[key]:
            text = text + repr(keyword[0]) + "," + repr(keyword[1]) + "," + repr(keyword[2]) + "," + repr(keyword[3]) + "\n"
        file = open(os.path.join(dir,key+".csv"),"w")
        file.writelines(text)
        file.flush()
        file.close()

def mi_based_keyword_extraction(dataset, top_k, config):
    mi_results = mi_fit(dataset, top_k)
    if len(config.class_correlation) > 0:
        mi_based_candidate_keywords = search(mi_results, dataset)
        mi_based_keywords = filterBySupport(mi_based_candidate_keywords, config.class_correlation[0],
                                            config.class_correlation[1], dataset.data_y)
        class_of_interest = config.class_correlation[2]
        filtred_keywords = computeOverlap(mi_results, mi_based_keywords[class_of_interest])
        return filtred_keywords, "mi"

def chi2_based_keyword_extraction(dataset, top_k, config):
    chi2_results = chi2_fit(dataset, top_k)
    if len(config.class_correlation) > 0:
        chi2_based_candidate_keywords = search(chi2_results, dataset)
        chi2_based_keywords = filterBySupport(chi2_based_candidate_keywords, config.class_correlation[0],
                                              config.class_correlation[1], dataset.data_y)
        class_of_interest = config.class_correlation[2]
        filtred_keywords = computeOverlap(chi2_results, chi2_based_keywords[class_of_interest])
        return filtred_keywords, "chi"

def tree_based_keyword_extraction(dataset, top_k, config):
    tree_results = tree_fit(dataset)
    if len(config.class_correlation) > 0:
        tree_based_candidate_keywords = search(tree_results, dataset)
        tree_based_keywords = filterBySupport(tree_based_candidate_keywords, config.class_correlation[0],
                                              config.class_correlation[1], dataset.data_y)
        class_of_interest = config.class_correlation[2]
        filtred_keywords = computeOverlap(tree_results, tree_based_keywords[class_of_interest])
        return filtred_keywords, "tree"

def keyword_extraction(approach, top_k, config, dataset):
    if approach == "chi":
        keywords,approach  = chi2_based_keyword_extraction(dataset, top_k, config)
        return keywords, approach
    if approach == "mi":
        keywords, approach = mi_based_keyword_extraction(dataset, top_k, config)
        return keywords, approach
    if approach == 'tree':
        keywords, approach = tree_based_keyword_extraction(dataset, top_k, config)
        return keywords, approach

def keyword_extraction_parallel(args):
    approach = args[0]
    top_k = args[1]
    config = args[2]
    dataset = args[3]
    if approach == "chi":
        chi2_based_keywords = chi2_based_keyword_extraction(dataset, top_k, config)
        return chi2_based_keywords, approach
    if approach == "mi":
        mi_based_keywords = mi_based_keyword_extraction(dataset, top_k, config)
        return mi_based_keywords, approach
    if approach == 'tree':
        tree_based_keywords = tree_based_keyword_extraction(dataset, top_k, config)
        return tree_based_keywords, approach

def main():
    args = parse_argument()
    print(args.data)
    logger = get_logger()
    config = read_configurations(args.config, logger)
    print(config['preprocess']['lemma'])
    original_dataset = Pipeliner.component_load_data(args, logger)
    print(original_dataset.data)
    original_dataset = Pipeliner.component_pre_process(original_dataset, config, logger)
    print(original_dataset.data)
    config = read_single_run_config(config, logger)
    dataset = Pipeliner.load_data_pipeline(original_dataset=original_dataset,
                                           data_id='independent',
                                           feature_family=config.feature_family,
                                           extraction_strategy=config.extraction_strategy,
                                           reduction_strategy=config.reduction_strategy,
                                           k=config.k,
                                           logger=logger)

    keywords = {}
    if args.jobs > 0:
        data_pairs = []
        for i in range(0, len(config.reduction_strategy)):
            data_pair = []
            approach = config.reduction_strategy[i]
            top_k = config.k[i]
            data_pair.append(approach)
            data_pair.append(int(top_k))
            data_pair.append(config)
            data_pair.append(dataset)
            data_pairs.append(data_pair)

        with closing(Pool(processes=args.jobs)) as p:
            results = p.map(keyword_extraction_parallel, data_pairs)

        for result in results:
            print(result[1])
            print(result[0])
            keywords[result[1]] = result[0][0]

    if os.path.exists(args.report):
        shutil.rmtree(args.report)
    os.makedirs(args.report)
    save_results(keywords,args.report)

if __name__ == '__main__':
    main()
