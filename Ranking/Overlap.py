def computeOverlap(feature_importance, feature_support):
    features = []
    dict = {}
    for feature in feature_importance:
        dict[feature[0]] = feature[1]
    #print(dict)
    for feature in feature_support:
        if feature[0] in dict.keys():
            feature_info = (feature[0], dict[feature[0]], int(feature[1]), int(feature[2]))
            features.append(feature_info)
    sorted_features = sorted(features, key=lambda x: x[2], reverse=True)
    return sorted_features