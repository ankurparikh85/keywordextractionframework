class Dataset:

    def __init__(self, data=None,
                 data_x=None, data_y=None, feature_names=None, data_text=None,
                 class_mapping=None,
                 data_id=None):

        self.data_id = data_id
        self.data = data
        self.data_x, self.data_y = data_x, data_y
        self.class_mapping = class_mapping
        self.feature_names = feature_names
        self.data_text = data_text