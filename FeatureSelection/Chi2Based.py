from sklearn.feature_selection import SelectKBest, chi2

def fit(dataset, k):
    selector = SelectKBest(chi2, k=k)
    #dataset.data_x = selector.fit_transform(dataset.data_x, dataset.data_y)
    selector.fit_transform(dataset.data_x, dataset.data_y)
    feature_names = [dataset.feature_names[i] for i in selector.get_support(indices=True)]
    feature_scores = list(zip(feature_names, selector.scores_))
    sorted_feature_scores = sorted(feature_scores, key=lambda x: x[1], reverse=True)
    #print(sorted_feature_scores)
    return sorted_feature_scores

