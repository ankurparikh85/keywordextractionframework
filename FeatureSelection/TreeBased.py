from sklearn.ensemble import RandomForestClassifier

def fit(dataset):
    selector = RandomForestClassifier(n_estimators=10000, random_state=0)
    selector = selector.fit(dataset.data_x, dataset.data_y)
    feature_scores = zip(dataset.feature_names, selector.feature_importances_)
    sorted_feature_scores = sorted(feature_scores, key=lambda s: s[1], reverse=True)
    return sorted_feature_scores


