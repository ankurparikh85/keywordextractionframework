from argparse import ArgumentParser
import os


def parse_argument():

    parser = ArgumentParser(description='Option parser for the generalized classification framework.')

    # add arguments
    parser.add_argument('-d', '--data',
                        type=str, required=True,
                        help='path to the data (csv format)')
    parser.add_argument('-c', '--config',
                        type=str, default=os.path.join('Resources', 'config.ini'),
                        required=True,
                        help='path to the .ini file to parse for parameter values.')
    parser.add_argument('-r', '--report',
                        type=str, required=True,
                        help='path to the directory to store the final report')
    parser.add_argument('-j', '--jobs',
                        type=int, default=1,
                        help='number of processes to run in parallel')

    args = parser.parse_args()

    return args