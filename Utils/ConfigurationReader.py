from configparser import ConfigParser
import sys

class Config:

    def __init__(self, feature_family=None, extraction_strategy=None,
                 reduction_strategy=None, k=None, class_correlation=None):

        self.feature_family = feature_family
        self.extraction_strategy = extraction_strategy
        self.reduction_strategy = reduction_strategy
        self.class_correlation = class_correlation
        self.k = k

def read(file_path, logger):
    logger.info('Reading configurations from {}'.format(file_path))
    config = ConfigParser()
    config.read(file_path)
    return config

def read_grid_config(config):

    feature_family, feature_extraction, feature_reduction, class_correlation = [], [], [], []

    feature_family_options = config['featureFamily']
    if feature_family_options['tf'] == 'True':
        feature_family.append('tf')
    if feature_family_options['tfidf'] == 'True':
        feature_family.append('tfidf')

    feature_extraction_options = config['featureExtraction']
    if feature_extraction_options['uni'] == 'True':
        feature_extraction.append([1, 1])
    if feature_extraction_options['bi'] == 'True':
        feature_extraction.append([2, 2])
    if feature_extraction_options['tri'] == 'True':
        feature_extraction.append([3, 3])
    if feature_extraction_options['uni-bi'] == 'True':
        feature_extraction.append([1, 2])
    if feature_extraction_options['bi-tri'] == 'True':
        feature_extraction.append([2, 3])
    if feature_extraction_options['uni-bi-tri'] == 'True':
        feature_extraction.append([1, 2, 3])

    feature_reduction_options = config['featureReduction']
    if feature_reduction_options['chi'] == 'True':
        feature_reduction.append(('chi', [int(entry) for entry in feature_reduction_options['n_chi'].split()]))
    if feature_reduction_options['mi'] == 'True':
        feature_reduction.append(('mi', [int(entry) for entry in feature_reduction_options['n_mi'].split()]))
    if feature_reduction_options['tree'] == 'True':
        feature_reduction.append(('tree', [int(entry) for entry in feature_reduction_options['n_tree'].split()]))

    if 'classCorrelation' in config:
        class_correlation_options = config['classCorrelation']
        if "class_min_support" in class_correlation_options:
            class_correlation.append(class_correlation_options["class_min_support"])
        if "other_class_max_support" in class_correlation_options:
            class_correlation.append(class_correlation_options["other_class_max_support"])
        if "class_of_interest" in class_correlation_options:
            class_correlation.append(class_correlation_options["class_of_interest"])

    return feature_family, feature_extraction, feature_reduction, class_correlation

def read_single_run_config(config, logger):

    feature_family, feature_extraction, feature_reduction, class_correlation = read_grid_config(config)
    k = []
    reduction_approaches = []
    if len(feature_family)>1 or len(feature_extraction)>1:
        logger.error('More than one options marked as True. Make the appropriate changes')
        sys.exit(1)
    if len(feature_family)==0 or len(feature_extraction)==0 or len(feature_reduction)==0:
        logger.error('None of the options marked as True. Make the appropriate changes')
        sys.exit(1)
    for reduction_approach in feature_reduction:
        reduction_approaches.append(reduction_approach[0])
        k.append(reduction_approach[1][0])

    '''    
    feature_reduction = feature_reduction[0]
    if len(feature_reduction[1])==0 and feature_reduction[0] != 'tree':
        logger.error('Values for parameter k not specified for the marked option.')
        sys.exit(1)

    if len(feature_reduction[1]) > 0:
        feature_reduction = (feature_reduction[0], feature_reduction[1][0])
    else:
        feature_reduction = (feature_reduction[0], None)
    '''
    config_object = Config(feature_family=feature_family[0],
                           extraction_strategy=feature_extraction[0],
                           reduction_strategy=reduction_approaches,
                           k=k,
                           class_correlation = class_correlation)
    return config_object
