import pandas as pd
from Components.Dataset import Dataset
from PreProcess.Lemmatizator import process as preprocess_lemmatization
from PreProcess.StopWordRemover import process as preprocess_stopwords
from FeatureExtraction.CountBasedVectorizer import create_vectorizer as tf_vectorizer
from FeatureExtraction.TfidfBasedVectorizer import create_vectorizer as tfidf_vectorizer


def component_load_data(args, logger):
    logger.info('Loading the dataset')
    data = None
    if args.data:
        data = pd.read_csv(args.data)
        logger.info('\tdataset size: {}'.format(len(data)))

    dataset = Dataset(data=data)

    return dataset

def component_pre_process(dataset, config, logger):
    logger.info('Starting pre-processing.')
    if 'True' in config['preprocess']['lemma']:
        logger.info('\tStep: Lemmatization.')
        dataset.data = preprocess_lemmatization(dataset.data)

    if 'True' == config['preprocess']['stopword']:
        logger.info('\tStep: Stop word removal.')
        dataset.data = preprocess_stopwords(dataset.data)
    return dataset

def component_vector_conversion(dataset,
                                feature_family,
                                extraction_strategy,
                                logger):
    logger.info('Starting vector conversion')
    if 'tf' == feature_family:
        logger.info('\ttf vectorizer.')
        dataset, vectorizer = tf_vectorizer(dataset, extraction_strategy)

    elif 'tfidf' == feature_family:
        logger.info('\ttf-idf vectorizer')
        dataset, vectorizer = tfidf_vectorizer(dataset, extraction_strategy)

    return dataset


def load_data_pipeline(original_dataset,
                       data_id,
                       feature_family,
                       extraction_strategy,
                       reduction_strategy,
                       k,
                       logger):

    dataset = Dataset(data=original_dataset.data, data_id=data_id)
    dataset = component_vector_conversion(dataset,
                                          feature_family,
                                          extraction_strategy,
                                          logger)
    #dataset = component_class_mapping(dataset, logger)
    #dataset = component_subset_selection(dataset, feature_family, reduction_strategy, k, logger)
    return dataset
