from sklearn.feature_extraction.text import CountVectorizer
import numpy as np


def load_only_text(data):

    all_texts = []
    labels = []
    for idx, entry in data.iterrows():
        all_texts.append(entry.text)
        labels.append(entry.label)
    return all_texts, labels



def create_vectorizer(dataset, extraction_strategy):

    ngram_start = int(np.min(extraction_strategy))
    ngram_end = int(np.max(extraction_strategy))
    vectorizer = CountVectorizer(lowercase=True, ngram_range=(ngram_start, ngram_end))
    all_texts, labels = load_only_text(dataset.data)
    texts = vectorizer.fit_transform(all_texts)
    feature_names = vectorizer.get_feature_names()
    #texts, labels = load_data(dataset.train, vectorizer, num_transcripts)
    #dataset.data_x = sparse.csr_matrix(np.array(texts))
    dataset.data_x = texts
    dataset.data_y = labels
    dataset.data_text = all_texts
    dataset.feature_names = feature_names
    return dataset, vectorizer