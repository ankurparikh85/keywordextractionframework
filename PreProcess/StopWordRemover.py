from nltk.corpus import stopwords

def process(data):

    if data is None:
        return data

    stopword_dict = set(stopwords.words('english'))
    texts = []
    for idx, row in data.iterrows():
        texts.append(row.text)

    for idx, text in enumerate(texts):
        texts[idx] = ' '.join([word for word in text.split() if not word in stopword_dict])

    data['text'] = texts

    return data

