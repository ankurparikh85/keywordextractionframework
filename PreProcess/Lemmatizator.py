from nltk.stem import WordNetLemmatizer

def process(data):

    if data is None:
        return data

    lemmatizer = WordNetLemmatizer()
    texts = []
    for idx, entry in data.iterrows():
        texts.append(entry.text)

    for idx, text in enumerate(texts):
        texts[idx] = ' '.join([lemmatizer.lemmatize(word.decode('utf-8')) for word in text.split()])

    data['text'] = texts
    return data