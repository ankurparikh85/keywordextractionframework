Keyword Extraction Framework
This module can be used to mine keywords automatically from the text. It uses various statistical Feature Selection approaches along with class frequencies based heuristics.

