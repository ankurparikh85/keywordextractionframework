import re

def search(features, dataset):
    class_distribution = {}
    for word in features:
        for i in range(0, len(dataset.data_text)):
            m = re.search('(^|\W)' + word[0] + '($|\W)', dataset.data_text[i].lower())
            if not m:
                continue
            if word[0] in class_distribution:
                classes = class_distribution.get(word[0])
                if dataset.data_y[i] in classes:
                    classes[dataset.data_y[i]] = classes[dataset.data_y[i]] + 1
                    class_distribution[word[0]] = classes
                else:
                    classes[dataset.data_y[i]] = 1
                    class_distribution[word[0]] = classes
            else:
                classes = {}
                classes[dataset.data_y[i]] = 1
                class_distribution[word[0]] = classes
    #print("Dictionary " + repr(class_distribution))
    return class_distribution
