
def filterBySupport(candidates, class_min_support, other_class_max_support, labels):
    class_keywords = {}
    for label in labels:
        keywords = []
        for key in candidates.keys():
            classes = candidates.get(key)
            if len(classes) == 1:
                if label in classes:
                    #print(key + "," + repr(classes[label]))
                    keyword = (key,repr(classes[label]),repr(0))
                    #keywords.append(key + "," + repr(classes[label]) + "," + repr(0) + "\n")
                    keywords.append(keyword)
            else:
                if label in classes and int(classes[label]) > class_min_support:
                    other_class_support = 0
                    for cls in classes:
                        if cls != label:
                            other_class_support = other_class_support + classes[cls]
                    if int(other_class_support) < other_class_max_support:
                        #print(key + "," + repr(classes[label]) + "," + repr(other_class_support))
                        keyword = (key,repr(classes[label]),repr(other_class_support))
                        #keywords.append(key + "," + repr(classes[label]) + "," + repr(other_class_support) + "\n")
                        keywords.append(keyword)
        #print(keywords)
        class_keywords[label] = keywords
    return class_keywords